package ch.briggen.bfh.sparklist.domain;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

import ch.briggen.bfh.sparklist.domain.shop.Item;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class ItemTest {

	@Test
	void testEmptyContructorYieldsEmptyObject() {
		Item i = new Item();
		assertThat("Id",i.getId(),is(equalTo(0l)));
		assertThat("Name",i.getName(),is(equalTo(null)));
		assertThat("Quantity",i.getQuantity(),is(equalTo(0)));
	}
	
	@ParameterizedTest
	@CsvSource({"1,One,-1","2,Two,0","3,Three,1"})
	void testContructorAssignsAllFields(long id,String name, String imagepath,int quantity, double price) {
		Item i = new Item(id, name, imagepath,quantity,price);
		assertThat("Id",i.getId(),equalTo(id));
		assertThat("Name",i.getName(),equalTo(name));
		assertThat("Quantity",i.getQuantity(),equalTo(quantity));
	}
	
	@ParameterizedTest
	@CsvSource({"1,One,-1","2,Two,0","3,Three,1"})
	void testSetters(long id,String name, int quantity) {
		Item i = new Item();
		i.setId(id);
		i.setName(name);
		i.setQuantity(quantity);
		assertThat("Id",i.getId(),equalTo(id));
		assertThat("Name",i.getName(),equalTo(name));
		assertThat("Quantity",i.getQuantity(),equalTo(quantity));
	}
	
	@Test
	void testToString() {
		Item i = new Item();
		System.out.println(i.toString());
		assertThat("toString smoke test",i.toString(),not(nullValue()));
	}
}
