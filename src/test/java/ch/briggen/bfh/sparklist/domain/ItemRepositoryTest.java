package ch.briggen.bfh.sparklist.domain;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import static ch.briggen.bfh.sparklist.domain.ItemDBTestHelper.*;


import java.util.Collection;
import java.util.NoSuchElementException;


import ch.briggen.bfh.sparklist.domain.shop.Item;
import ch.briggen.bfh.sparklist.domain.shop.ItemRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;


class ItemRepositoryTest {

	ItemRepository repo = null;

	
	private static void populateRepo(ItemRepository r) {
		for(int i = 0; i <10; ++i) {
			Item dummy = new Item(0,"Fake Test Item" + i,"/image/test.png",i,i);
			r.insert(dummy);
		}
	}
	
	@BeforeEach
	void setUp() throws Exception {
		String randomName = "Test@"+System.currentTimeMillis();
		initDataSourceForTest(randomName);
		initDB();
		repo = new ItemRepository();
	}

	@Test
	void testEmptyDB() {
		assertThat("New DB must be empty",repo.getAll().isEmpty(),is(true));
	}
	
	@Test
	void testPopulatedDB()
	{
		populateRepo(repo);
		assertThat("Freshly populated DB must hold 10 ites",repo.getAll().size(),is(10));
	}
	
	@ParameterizedTest
	@CsvSource({"1,One,-1","2,Two,0","3,Three,1"})
	void testInsertItems(long id,String name, String imagepath, int quantity, double price)
	{
		populateRepo(repo);
		
		Item i = new Item(id,name,imagepath,quantity,price);
		long dbId = repo.insert(i);
		Item fromDB = repo.getById(dbId);
		assertThat("id = id", fromDB.getId(),is(dbId) );
		assertThat("name = name", fromDB.getName(),is(name) );
		assertThat("imagepath = imagepath", fromDB.getName(),is(imagepath) );
		assertThat("count = count", fromDB.getQuantity(),is(quantity) );
		assertThat("price = price", fromDB.getPrice(),is(price) );

	}
	
	@ParameterizedTest
	@CsvSource({"1,One,-1,Eins,-2","2,Two,0,Zwei,-1","3,Three,1,Drei,0"})
	void testUpdateItems(long id,String name,String imagepath,int quantity,double price,String newName,int newQty, double newPrice)
	{
		populateRepo(repo);
		
		Item i = new Item(id,name,imagepath,quantity,price);
		long dbId = repo.insert(i);
		i.setId(dbId);
		i.setName(newName);
		i.setQuantity(newQty);
		i.setPrice(newPrice);
		repo.save(i);
		
		Item fromDB = repo.getById(dbId);
		assertThat("id = id", fromDB.getId(),is(dbId) );
		assertThat("name = name", fromDB.getName(),is(newName) );
		assertThat("count = count", fromDB.getQuantity(),is(newQty) );
		assertThat("price = price", fromDB.getPrice(),is(newPrice) );
		
	}
	
	@ParameterizedTest
	@CsvSource({"1,One,-1","2,Two,0","3,Three,1"})
	void testDeleteItems(long id,String name,String imagepath,int quantity,double price)
	{
		populateRepo(repo);
		
		Item i = new Item(id,name,imagepath,quantity,price);
		long dbId = repo.insert(i);
		Item fromDB = repo.getById(dbId);
		assertThat("Item was written to DB",fromDB,not(nullValue()));
		repo.delete(dbId);
		assertThrows(NoSuchElementException.class, ()->{repo.getById(dbId);},"Item should have been deleted");
	}
	
	@Test
	void testDeleteManyRows()
	{
		populateRepo(repo);
		for(Item i : repo.getAll())
		{
			repo.delete(i.getId());
		}
		
		assertThat("DB must be empty after deleteing all items",repo.getAll().isEmpty(),is(true));
	}
	
	@ParameterizedTest
	@CsvSource({"1,One,-1","2,Two,0","3,Three,1"})
	void testGetByOneName(long id,String name,String imagepath,int quantity, double price)
	{
		populateRepo(repo);
		
		Item i = new Item(id,name,imagepath,quantity, price);
		repo.insert(i);
		Collection<Item> fromDB = repo.getByName(name);
		assertThat("Exactly one item was returned", fromDB.size(),is(1));
		
		Item elementFromDB = fromDB.iterator().next();
		
		assertThat("name = name", elementFromDB.getName(),is(name) );
		assertThat("count = count", elementFromDB.getQuantity(),is(quantity) );
		assertThat("price = price", elementFromDB.getPrice(),is(price) );
	}
	
	@ParameterizedTest
	@CsvSource({"1,One,-1","2,Two,0","3,Three,1"})
	void testGetManyItemsByName(int count,String name,String imagepath,int quantity, double price)
	{
		populateRepo(repo);
		
		
		for(int n = 0; n < count; ++n) {
			Item i = new Item(0,name,imagepath,quantity,price);
			repo.insert(i);
		}
		
		Collection<Item> fromDB = repo.getByName(name);
		assertThat("Exactly one item was returned", fromDB.size(),is(count));
		
		for(	Item elementFromDB : fromDB)
		{
			assertThat("name = name", elementFromDB.getName(),is(name) );
			assertThat("count = count", elementFromDB.getQuantity(),is(quantity) );
			assertThat("price = price", elementFromDB.getPrice(),is(price) );
		}
	}
	
	@Test
	void testGetNoItemsByName()
	{
		populateRepo(repo);
		
		Collection<Item> fromDB = repo.getByName("NotExistingItem");
		assertThat("Exactly one item was returned", fromDB.size(),is(0));
		
	}
}
