package ch.briggen.bfh.sparklist;

import static spark.Spark.get;
import static spark.Spark.post;

import java.util.Map.Entry;

import ch.briggen.bfh.sparklist.web.impressum.ImpressumController;
import ch.briggen.bfh.sparklist.web.login.LoginRootController;
import ch.briggen.bfh.sparklist.web.login.LoginValidationController;
import ch.briggen.bfh.sparklist.web.login.LogoutController;
import ch.briggen.bfh.sparklist.web.register.NewRegistrationController;
import ch.briggen.bfh.sparklist.web.register.RegisterController;
import ch.briggen.bfh.sparklist.web.shop.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.sparkbase.H2SparkApp;
import ch.briggen.sparkbase.UTF8ThymeleafTemplateEngine;
import spark.TemplateViewRoute;

public class SparkListServer extends H2SparkApp {

	final static Logger log = LoggerFactory.getLogger(SparkListServer.class);

	public static void main(String[] args) {
		
		for(Entry<Object, Object> property: System.getProperties().entrySet()) {
			log.debug(String.format("Property %s : %s", property.getKey(),property.getValue()));
		}

		SparkListServer server = new SparkListServer();
		server.configure();
		server.run();
	}

	@Override
	protected void doConfigureHttpHandlers() {
		post("/searchstore", new SearchController(), new UTF8ThymeleafTemplateEngine());
		get("/login", new LoginRootController(), new UTF8ThymeleafTemplateEngine());
		post("/login", new LoginRootController(), new UTF8ThymeleafTemplateEngine());
		post("/login/validation", new LoginValidationController(), new UTF8ThymeleafTemplateEngine());
		get("/logout", new LogoutController(), new UTF8ThymeleafTemplateEngine());
		get("/register", new RegisterController(), new UTF8ThymeleafTemplateEngine());
		post("/register/new", new NewRegistrationController(), new UTF8ThymeleafTemplateEngine());
		get("/aboutus", new ImpressumController(), new UTF8ThymeleafTemplateEngine());
		get("/", new ListManagementRootController(), new UTF8ThymeleafTemplateEngine());
		get("/item", new ItemEditController(), new UTF8ThymeleafTemplateEngine());
		post("/item/update", new ItemUpdateController(), new UTF8ThymeleafTemplateEngine());
		get("/item/delete", new ItemDeleteController(), new UTF8ThymeleafTemplateEngine());
		post("/item/new", new ItemNewController(), new UTF8ThymeleafTemplateEngine());
		get("/shoppingcart", new ShoppingcartController(), new UTF8ThymeleafTemplateEngine());
		get("/shoppingcart/delete", new ShoppingcartDeleteController(), new UTF8ThymeleafTemplateEngine());
		get("/shoppingcart/viewer", new ShoppingCartViewerController(), new UTF8ThymeleafTemplateEngine());
		get("/shoppingcart/all/delete", new ShoppingCartMultipleDeleteController(), new UTF8ThymeleafTemplateEngine());

	}

}
