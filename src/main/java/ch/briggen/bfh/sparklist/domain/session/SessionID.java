package ch.briggen.bfh.sparklist.domain.session;

public class SessionID {

    private int u_id;

    /**
     * Default Constructor for use is a controller
     */
    public SessionID() {

    }

    /**
     * Constructor for SessionIDItem
     * @param u_id = ID of the user
     */
    public SessionID(int u_id) {
        this.u_id = u_id;
    }

    /**
     * Getter and Setter for controller use
     * @return
     */

    public int getId() {
        return u_id;
    }

    public void setId(int userid) {
        this.u_id = userid;
    }

    @Override
    public String toString() {
        return String.format("Session:{userid: %d;}", u_id);
    }

}
