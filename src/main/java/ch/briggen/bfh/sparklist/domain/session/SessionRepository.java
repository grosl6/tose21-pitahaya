package ch.briggen.bfh.sparklist.domain.session;

import ch.briggen.bfh.sparklist.domain.shop.ItemRepository;
import ch.briggen.bfh.sparklist.domain.util.RepositoryException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import static ch.briggen.bfh.sparklist.domain.util.JdbcRepositoryHelper.getConnection;


public class SessionRepository {

    private final static Logger log = LoggerFactory.getLogger(ItemRepository.class);

    /**
     * Converts result into a session collection
     * @param rs = Result SQL Statement
     * @return
     * @throws SQLException
     */
    private static Collection<Session> mapSessions(ResultSet rs) throws SQLException {
        LinkedList<Session> list = new LinkedList<Session>();
        while (rs.next()) {
            Session i = new Session(rs.getInt("id"), rs.getString("name"));
            list.add(i);
        }
        return list;
    }

    /**
     * Returns all Sessions wit user id in the database
     * @return
     */
    public Collection<Session> getAll() {
        try (Connection conn = getConnection()) {
            PreparedStatement stmt = conn.prepareStatement("select SE_ID, U_ID from SESSIONS");
            ResultSet rs = stmt.executeQuery();
            return mapSessions(rs);
        } catch (SQLException e) {
            String msg = "SQL error while retrieving all Sessions. ";
            log.error(msg, e);
            throw new RepositoryException(msg);
        }
    }

    /**
     * Updates the session in the database
     * @param i = Session item
     */
    public void save(Session i) {

        try (Connection conn = getConnection()) {
            PreparedStatement stmt = conn.prepareStatement("update SESSIONS set U_ID=? where SE_ID=?");
            stmt.setString(1, i.getName());
            stmt.setInt(2, i.getId());
            stmt.executeUpdate();
        } catch (SQLException e) {
            String msg = "SQL error while updating UserSessions " + i;
            log.error(msg, e);
            throw new RepositoryException(msg);
        }

    }

    /**
     * Deletes all Sessions
     * @param i = Session item
     * @return
     */

    public Object delete(Session i) {

        try (Connection conn = getConnection()) {
            PreparedStatement stmt = conn.prepareStatement("delete from SESSIONS");
            stmt.executeUpdate();
        } catch (SQLException e) {
            String msg = "SQL error while deleting UserSessions by id ";
            log.error(msg, e);
            throw new RepositoryException(msg);
        }
        return null;
    }

    /**
     * Inserts a new Session into the database
     * @param i = Session item
     * @return
     */

    public int insert(String i) {
        log.trace("insert " + i);

        try (Connection conn = getConnection()) {
            PreparedStatement stmt = conn.prepareStatement("insert into SESSIONS (U_ID) values (?)", PreparedStatement.RETURN_GENERATED_KEYS);
            stmt.setString(1, i);
            stmt.executeUpdate();
            ResultSet key = stmt.getGeneratedKeys();
            key.next();
            int id = key.getInt(1);
            return id;
        } catch (SQLException e) {
            String msg = "SQL error while inserting Sessions " + i;
            log.error(msg, e);
            throw new RepositoryException(msg);
        }
    }
}