package ch.briggen.bfh.sparklist.domain.user;

import ch.briggen.bfh.sparklist.domain.util.RepositoryException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Base64;
import java.util.Collection;
import java.util.LinkedList;

import static ch.briggen.bfh.sparklist.domain.util.JdbcRepositoryHelper.getConnection;

public class UserRepo {

    private final Logger log = LoggerFactory.getLogger(UserRepo.class);
    String USERID;

    /**
     * Returns all users in the database
     * @return
     */

    public Collection<User> getAllUsers() {
        try (Connection conn = getConnection()) {
            PreparedStatement stmt = conn.prepareStatement("SELECT U_ID, U_FIRSTNAME, U_LASTNAME, U_EMAIL, U_PASSWORD FROM USER");
            ResultSet rs = stmt.executeQuery();
            return mapUsers(rs);
        } catch (SQLException e) {
            String msg = "SQL error while retrieving all users. ";
            log.error(msg, e);
            throw new RepositoryException(msg);
        }
    }

    /**
     * Returns the user with the ID
     * @param id = ID of the user
     * @return
     */

    public User getUserById(long id) {
        try (Connection conn = getConnection()) {
            PreparedStatement stmt = conn.prepareStatement("SELECT U_ID, U_FIRSTNAME, U_LASTNAME, U_EMAIL, U_PASSWORD from USER where U_ID=?");
            stmt.setLong(1, id);
            ResultSet rs = stmt.executeQuery();
            return mapUsers(rs).iterator().next();
        } catch (SQLException e) {
            String msg = "SQL error while retrieving items by id " + id;
            log.error(msg, e);
            throw new RepositoryException(msg);
        }
    }

    /**
     * Returns the user with the email
     * @param email = Email of the user
     * @return
     */

    public String getUserIDByEmail(String email) {
        try (Connection conn = getConnection()) {
            PreparedStatement stmt = conn.prepareStatement("SELECT U_ID from USER where U_EMAIL=?");
            stmt.setString(1, email);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                USERID = rs.getString("U_ID");
            }
            return USERID;
        } catch (SQLException e) {
            String msg = "SQL error while retrieving items by id " + email;
            log.error(msg, e);
            throw new RepositoryException(msg);
        }
    }

    /**
     * Converts result into a user collection
     * @param rs = Result SQL Statement
     * @return
     * @throws SQLException
     */

    private Collection<User> mapUsers(ResultSet rs) throws SQLException {
        LinkedList<User> list = new LinkedList<User>();
        while (rs.next()) {
            User u = new User(rs.getInt("U_ID"), rs.getString("U_FIRSTNAME"), rs.getString("U_LASTNAME"), rs.getString("U_EMAIL"), rs.getString("U_PASSWORD"));
            list.add(u);
        }
        return list;
    }

    /**
     * Validates if the email and password are correct and matches the entery in the database
     * @param email = Email of the user
     * @param password = Password of the user
     * @return
     */

    public boolean validateUser(String email, String password) throws Exception {
        try (Connection conn = getConnection()) {
            PreparedStatement stmt = conn.prepareStatement("SELECT U_EMAIL, U_PASSWORD from USER WHERE U_EMAIL=? AND U_PASSWORD=?");
            stmt.setString(1, email);
            stmt.setString(2, encryptedPassword(password,"BFHFS21!TOSE21"));
            ResultSet rs = stmt.executeQuery();
            if (!rs.next()) {
                return false;
            } else {
                System.out.println("Login successful");
                return true;
            }
        } catch (SQLException e) {
            String msg = "SQL error while retrieving all users. ";
            log.error(msg, e);
            throw new RepositoryException(msg);
        }

    }

    /**
     * Encryts the password from the user
     * @param strKey = Encryptionkey
     * @param password = Password from the user
     * @return
     * @throws Exception
     */

    private static String encryptedPassword(String strKey, String password) throws Exception {
        String strData="";
        byte[] keyData = (strKey+password).getBytes();
        SecretKeySpec secretKeySpec = new SecretKeySpec(keyData, "Blowfish");
        Cipher cipher = Cipher.getInstance("Blowfish");
        cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec);
        byte[] encrypted = cipher.doFinal(password.getBytes());
        strData=Base64.getEncoder().encodeToString(encrypted);
        return strData;
    }

    /**
     * Inserts new user into the database
     * @param u = User Item
     * @return
     * @throws Exception
     */

    public long insertUser(User u) throws Exception {
        try (Connection conn = getConnection()) {
            PreparedStatement stmt = conn.prepareStatement("INSERT INTO USER (U_FIRSTNAME, U_LASTNAME, U_EMAIL, U_PASSWORD) values (?,?,?,?)", PreparedStatement.RETURN_GENERATED_KEYS);
            stmt.setString(1, u.getFirstName());
            stmt.setString(2, u.getLastName());
            stmt.setString(3, u.getEmail());
            stmt.setString(4, encryptedPassword(u.getPassword(),"BFHFS21!TOSE21"));
            stmt.executeUpdate();
            ResultSet key = stmt.getGeneratedKeys();
            key.next();
            Long id = key.getLong(1);
            return id;
        } catch (SQLException e) {
            String msg = "SQL error while inserting user " + u;
            log.error(msg, e);
            throw new RepositoryException(msg);
        }
    }

    /**
     * Checks if email is allready in the database
     * @param email = Email of the user
     * @return
     */

    public boolean checkIfEmailExists(String email) {
        try (Connection conn = getConnection()) {
            PreparedStatement stmt = conn.prepareStatement("SELECT * FROM USER WHERE U_EMAIL=?");
            stmt.setString(1, email);
            ResultSet rs = stmt.executeQuery();
            return rs.next();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return true;
        }
    }

}

