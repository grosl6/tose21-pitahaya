package ch.briggen.bfh.sparklist.domain.util;

import ch.briggen.sparkbase.JdbcSparkApp;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

public class JdbcRepositoryHelper {

    private static DataSource alternativeDataSource = null;

    public static synchronized void overrideDefalutDataSource(DataSource javaxDataSource) {
        alternativeDataSource = javaxDataSource;
    }

    public static synchronized Connection getConnection() throws SQLException {
        if (null == alternativeDataSource) {
            return JdbcSparkApp.getApplication().getJdbcConnection();
        } else {
            return alternativeDataSource.getConnection();
        }
    }

}
