package ch.briggen.bfh.sparklist.domain.session;


public class Session {

    private int id;
    private String name;

    /**
     * Default Constructor for use is a controller
     */
    public Session() { }

    /**
     * Constructor for SessionItem
     * @param id =ID of the user
     * @param name = Name of the user
     */
    public Session(int id, String name) {
        this.id = id;
        this.name = name;
    }

    /**
     * Getter and Setter for controller use
     * @return
     */

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Override
    public String toString() {
        return String.format("Session:{id: %d; name: %s;}", name, id);
    }

}
