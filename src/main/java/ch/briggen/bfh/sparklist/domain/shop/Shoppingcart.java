package ch.briggen.bfh.sparklist.domain.shop;

import ch.briggen.bfh.sparklist.domain.user.User;

import java.util.ArrayList;

public class Shoppingcart {


    private long id;
    private User user;
    private ArrayList<Item> items;

    /**
     * Constructor for use in controller
     * @param id = ID of the shoppingcart
     * @param user = Shoppingcart of the user
     * @param items = List of all items
     */

    public Shoppingcart(long id, User user, ArrayList<Item> items) {
        this.id = id;
        this.user = user;
        this.items = items;
    }

    public Shoppingcart(long id, User user) {
        this.id = id;
        this.user = user;

    }

    /**
     * Getter and Setter fot controller use
     * @return
     */

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public ArrayList<Item> getItems() {
        return items;
    }

    public void setItems(ArrayList<Item> items) {
        this.items = items;
    }


    public double getTotalPrice() {
        double totalprice = 0;
        for (int i = 0; i < items.size(); i++) {
            totalprice = totalprice + items.get(i).getPrice();
        }
        return totalprice;
    }

    public int getTotalAmount() {
        return items.size();
    }

    public int getSpecificAmount(Item item) {
        int specificamount = 0;
        for (int i = 0; i < items.size(); i++) {
            if (items.get(i).equals(item)) {
                specificamount = specificamount + 1;
            }
        }
        return specificamount;
    }

    public double getSpecificPrice(Item item) {
        double specificprice = 0;
        for (int i = 0; i < items.size(); i++) {
            if (items.get(i).equals(item)) {
                specificprice = specificprice + item.getPrice();
            }
        }
        return specificprice;
    }


    public ArrayList<Item> getDistinctShoppingCart() {
        ArrayList<Item> filtereditems = new ArrayList<>();
        for (int i = 0; i < items.size(); i++) {

            if (!filtereditems.contains(items.get(i))) {
                filtereditems.add(items.get(i));
            }
        }
        return filtereditems;


    }


}
