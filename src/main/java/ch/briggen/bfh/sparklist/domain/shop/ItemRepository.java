package ch.briggen.bfh.sparklist.domain.shop;

import ch.briggen.bfh.sparklist.domain.util.RepositoryException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import static ch.briggen.bfh.sparklist.domain.util.JdbcRepositoryHelper.getConnection;

public class ItemRepository {

    private final Logger log = LoggerFactory.getLogger(ItemRepository.class);

    /**
     * Reterns all items form the Table "ITEMS"
     * @return Collection of items
     */

    public Collection<Item> getAll() {
        try (Connection conn = getConnection()) {
            PreparedStatement stmt = conn.prepareStatement("select I_ID, I_NAME, I_IMAGEPATH, I_QUANTITY, I_PRICE from items");
            ResultSet rs = stmt.executeQuery();
            return mapItems(rs);
        } catch (SQLException e) {
            String msg = "SQL error while retreiving all items. ";
            log.error(msg, e);
            throw new RepositoryException(msg);
        }
    }

    /**
     * Retuns all items that matches the search term
     * @param item = String in search box
     * @return
     */

    public Collection<Item> searchItem(String item) {
        try (Connection conn = getConnection()) {
            PreparedStatement stmt = conn.prepareStatement("select I_ID, I_NAME, I_IMAGEPATH, I_QUANTITY, I_PRICE from items where I_NAME like ?");
            stmt.setString(1, '%' + item + '%');
            System.out.println(stmt);
            ResultSet rs = stmt.executeQuery();
            return mapItems(rs);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return null;
        }
    }

    /**
     * Returns all items that matches the name
     * @param name = Name of the item
     * @return
     */

    public Collection<Item> getByName(String name) {
        try (Connection conn = getConnection()) {
            PreparedStatement stmt = conn.prepareStatement("select I_ID, I_NAME, I_IMAGEPATH, I_QUANTITY, I_PRICE from items where I_NAME=?");
            stmt.setString(1, name);
            ResultSet rs = stmt.executeQuery();
            return mapItems(rs);
        } catch (SQLException e) {
            String msg = "SQL error while retreiving items by name " + name;
            log.error(msg, e);
            throw new RepositoryException(msg);
        }

    }

    /**
     * Returns all items with the same ID
     * @param id = ID of the item
     * @return
     */

    public Item getById(long id) {
        try (Connection conn = getConnection()) {
            PreparedStatement stmt = conn.prepareStatement("select I_ID, I_NAME, I_IMAGEPATH, I_QUANTITY, I_PRICE from items where I_ID=?");
            stmt.setLong(1, id);
            ResultSet rs = stmt.executeQuery();
            return mapItems(rs).iterator().next();
        } catch (SQLException e) {
            String msg = "SQL error while retreiving items by id " + id;
            log.error(msg, e);
            throw new RepositoryException(msg);
        }
    }

    /**
     * Updates the item in the database
     * @param i = Item object
     */

    public void save(Item i) {
        try (Connection conn = getConnection()) {
            PreparedStatement stmt = conn.prepareStatement("update items set I_NAME=?, I_QUANTITY=?, I_PRICE=? where I_ID=?");
            stmt.setString(1, i.getName());
            stmt.setInt(2, i.getQuantity());
            stmt.setDouble(3, i.getPrice());
            stmt.setLong(4, i.getId());
            stmt.executeUpdate();
        } catch (SQLException e) {
            String msg = "SQL error while updating item " + i;
            log.error(msg, e);
            throw new RepositoryException(msg);
        }

    }

    /**
     * Deletes the item in the database which matches the id
     * @param id = ID of the Item
     */

    public void delete(long id) {
        try (Connection conn = getConnection()) {
            PreparedStatement stmt = conn.prepareStatement("delete from items where I_ID=?");
            stmt.setLong(1, id);
            stmt.executeUpdate();
        } catch (SQLException e) {
            String msg = "SQL error while deleteing items by id " + id;
            log.error(msg, e);
            throw new RepositoryException(msg);
        }
    }

    /**
     * Inserts new item into the database with all attributes
     * @param i = Item object
     * @return Returns the item ID
     */

    public long insert(Item i) {
        try (Connection conn = getConnection()) {
            PreparedStatement stmt = conn.prepareStatement("insert into items (I_NAME, I_QUANTITY, I_PRICE) values (?,?,?)", PreparedStatement.RETURN_GENERATED_KEYS);
            stmt.setString(1, i.getName());
            stmt.setInt(2, i.getQuantity());
            stmt.setDouble(3, i.getPrice());
            stmt.executeUpdate();
            ResultSet key = stmt.getGeneratedKeys();
            key.next();
            Long id = key.getLong(1);
            return id;
        } catch (SQLException e) {
            String msg = "SQL error while inserting item " + i;
            log.error(msg, e);
            throw new RepositoryException(msg);
        }
    }

    /**
     * Converts results into a item object
     * @param rs = Result SQL Statement
     * @return Collection of Items
     * @throws SQLException
     */

    private static Collection<Item> mapItems(ResultSet rs) throws SQLException {
        LinkedList<Item> list = new LinkedList<Item>();
        while (rs.next()) {
            Item i = new Item(rs.getLong("I_ID"), rs.getString("I_NAME"), rs.getString("I_IMAGEPATH"), rs.getInt("I_QUANTITY"), rs.getDouble("I_PRICE"));
            list.add(i);
        }
        return list;
    }
}
