package ch.briggen.bfh.sparklist.domain.shop;

public class Item {

    private long id;
    private String name;
    private String imagepath;
    private int quantity;
    private double price;

    /**
     * Default Constructor for use is a controller
     */
    public Item() {    }

    /**
     * Constructor for Item
     * @param I_ID = Unique Item ID
     * @param I_NAME = Name of the  Item
     * @param I_IMAGEPATH = Image path for the Item
     * @param I_QUANTITY = Quantity of the Item
     * @param I_PRICE = Price of the Item
     */

    public Item(long I_ID, String I_NAME, String I_IMAGEPATH, int I_QUANTITY, double I_PRICE) {
        this.id = I_ID;
        this.name = I_NAME;
        this.imagepath = I_IMAGEPATH;
        this.quantity = I_QUANTITY;
        this.price = I_PRICE;
    }

    /**
     * Getter and Setter for the controller
     * @return
     */

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return imagepath;
    }

    public void setPath(String imagepath) {
        this.imagepath = imagepath;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return String.format("Item:{id: %d; name: %s; imagepath: %s quantity: %d; price: %f }", id, name, imagepath, quantity, price);
    }

    @Override
    public boolean equals(Object obj) {

        if (obj == this) {
            return true;
        }

        if (!(obj instanceof Item)) {
            return false;
        }
        Item i = (Item) obj;

        return Double.compare(price, i.price) == 0 && name == i.name && imagepath == i.imagepath;

    }


}
