package ch.briggen.bfh.sparklist.domain.shop;

import ch.briggen.bfh.sparklist.domain.user.User;
import ch.briggen.bfh.sparklist.domain.user.UserRepo;
import ch.briggen.bfh.sparklist.domain.util.RepositoryException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import static ch.briggen.bfh.sparklist.domain.util.JdbcRepositoryHelper.getConnection;

public class ShoppingcartRepository {

    /**
     * Converts result into a shoppingcart item
     * @param rs = Result SQL Statement
     * @return
     * @throws SQLException
     */

    private static Shoppingcart mapShoppingcart(ResultSet rs) throws SQLException {
        ItemRepository itemRepository = new ItemRepository();
        UserRepo userRepo = new UserRepo();
        User user = null;
        ArrayList<Item> filtered_items = new ArrayList<>();
        Shoppingcart shoppingcart = null;
        while (rs.next()) {
            user = userRepo.getUserById(rs.getLong("U_ID"));
            Item item = itemRepository.getById(rs.getLong("I_ID"));
            filtered_items.add(item);
            shoppingcart = new Shoppingcart(rs.getLong("S_ID"), user, filtered_items);
        }
        return shoppingcart;
    }

    /**
     * Deletes the shoppingcart in the database
     * @param item_id = ID of the item
     */

    public void deleteShoppingcart(long item_id) {
        try (Connection conn = getConnection()) {
            PreparedStatement stmt = conn.prepareStatement("select S_ID, I_ID, U_ID from SHOPPING_CART where I_ID=?");
            stmt.setInt(1, (int) item_id);
            ResultSet rs = stmt.executeQuery();
            Shoppingcart shoppingcart = mapShoppingcart(rs);
            PreparedStatement stmt2 = conn.prepareStatement("delete from SHOPPING_CART where S_ID=?");
            stmt2.setLong(1, shoppingcart.getId());
            stmt2.executeUpdate();
        } catch (SQLException e) {
            String msg = "SQL error while deleteing items by id " + item_id;
            throw new RepositoryException(msg);
        }
    }

    /**
     * Deletes multiple items for the database
     * @param item_id = ID of the item
     */

    public void multipleDeleteShoppingcart(long item_id) {
        try (Connection conn = getConnection()) {
            PreparedStatement stmt = conn.prepareStatement("select S_ID, I_ID, U_ID from SHOPPING_CART where I_ID=?");
            stmt.setInt(1, (int) item_id);
            ResultSet rs = stmt.executeQuery();
            Shoppingcart shoppingcart = mapShoppingcart(rs);
            PreparedStatement stmt2 = conn.prepareStatement("delete from SHOPPING_CART where I_ID=?");
            stmt2.setLong(1, item_id);
            stmt2.executeUpdate();
        } catch (SQLException e) {
            String msg = "SQL error while deleteing items by id " + item_id;
            throw new RepositoryException(msg);
        }
    }

    /**
     * Creates a shoppingcart in the database for the user
     * @param item_id = ID of the item
     * @param user_id = ID of the user
     * @return
     */

    public Shoppingcart createShoppingcart(long item_id, long user_id) {
        try (Connection conn = getConnection()) {
            PreparedStatement stmt_hc2 = conn.prepareStatement("insert into SHOPPING_CART (I_ID, U_ID) values (?,?)");
            stmt_hc2.setInt(2, (int) user_id);
            stmt_hc2.setInt(1, (int) item_id);
            stmt_hc2.executeUpdate();
            PreparedStatement stmt = conn.prepareStatement("select S_ID, I_ID, U_ID from SHOPPING_CART where U_ID=?");
            stmt.setInt(1, (int) user_id);
            ResultSet rs = stmt.executeQuery();
            Shoppingcart shoppingcart = mapShoppingcart(rs);
            return shoppingcart;
        } catch (SQLException e) {
            throw new RepositoryException("error in createshoppingcart method");
        }

    }

    /**
     * Returns the shoppingcart which matches the user ID
     * @param user_id = ID of the user
     * @return
     */

    public Shoppingcart getShoppingcart(long user_id) {
        try (Connection conn = getConnection()) {
            PreparedStatement stmt = conn.prepareStatement("select S_ID, I_ID, U_ID from SHOPPING_CART where U_ID=?");
            stmt.setInt(1, (int) user_id);
            ResultSet rs = stmt.executeQuery();
            System.out.println(rs);
            Shoppingcart shoppingcart = mapShoppingcart(rs);
            return shoppingcart;
        } catch (SQLException e) {
            throw new RepositoryException("error in createshoppingcart method");
        }

    }

}
