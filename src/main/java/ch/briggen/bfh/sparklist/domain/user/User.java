package ch.briggen.bfh.sparklist.domain.user;

import ch.briggen.bfh.sparklist.domain.shop.Shoppingcart;

public class User {

    private long id;
    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private Shoppingcart shoppingcart;

    /**
     * Default Constructor for use is a controller
     */

    public User() { }

    /**
     * Constructor for UserItem
     * @param id = ID of the user
     * @param firstName = First name of the user
     * @param lastName= Last name of the user
     * @param email= Email of the user
     * @param password = Password of the user
     */

    public User(long id, String firstName, String lastName, String email, String password) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
    }

    /**
     * Getter and Setter for constructor use
     * @param email
     * @param password
     */

    public User(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public Shoppingcart getShoppingcart() {
        return shoppingcart;
    }

    public void setShoppingcart(Shoppingcart shoppingcart) {
        this.shoppingcart = shoppingcart;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
