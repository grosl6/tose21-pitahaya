package ch.briggen.bfh.sparklist.web.shop;

import ch.briggen.bfh.sparklist.domain.shop.Shoppingcart;
import ch.briggen.bfh.sparklist.domain.shop.ShoppingcartRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

import java.util.HashMap;

public class ShoppingCartMultipleDeleteController implements TemplateViewRoute {

    ShoppingcartRepository repository = new ShoppingcartRepository();


    @Override
    public ModelAndView handle(Request request, Response response) throws Exception {
        long user_id = 1;
        long item_id = Long.parseLong(request.queryParams("id"));
        repository.multipleDeleteShoppingcart(item_id);

        Shoppingcart shoppingcart = repository.getShoppingcart(user_id);

        HashMap<String, Shoppingcart> model = new HashMap<String, Shoppingcart>();
        if (shoppingcart == null){
            response.redirect("/");
            return null;
        }

        else {
            model.put("items", shoppingcart);
            return new ModelAndView(model, "shoppingListTemplate");
        }

    }

}
