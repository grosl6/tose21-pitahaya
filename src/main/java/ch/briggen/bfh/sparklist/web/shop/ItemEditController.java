package ch.briggen.bfh.sparklist.web.shop;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.shop.Item;
import ch.briggen.bfh.sparklist.domain.shop.ItemRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für alle Operationen auf einzelnen Items
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Marcel Briggen
 *
 */

public class ItemEditController implements TemplateViewRoute{
	
	private final Logger log = LoggerFactory.getLogger(ItemEditController.class);
	
	
	
	private ItemRepository itemRepo = new ItemRepository();
	
	
	/**
	 * Requesthandler zum Bearbeiten eines Items. 
	 * Liefert das Formular (bzw. Template) zum bearbeiten der einzelnen Felder
	 * Wenn der id Parameter 0 ist wird beim submitten des Formulars ein neues Item erstellt (Aufruf von /item/new)
	 * Wenn der id Parameter <> 0 ist wird beim submitten des Formulars das Item mit der übergebenen id upgedated (Aufruf /item/save)
	 * Hört auf GET /item
	 * @return gibt den Namen des zu verwendenden Templates zurück. Immer "itemDetailTemplate" .
	 */
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String idString = request.queryParams("id");
		HashMap<String, Object> model = new HashMap<String, Object>();

		if (request.cookie("loggedin") == null) {
			model.put("isLoggedin", "false");
			if(null == idString)
			{
				model.put("postAction", "/item/new");
				model.put("itemDetail", new Item());
			}
			else
			{
				model.put("postAction", "/item/update");
				Long id = Long.parseLong(idString);
				Item i = itemRepo.getById(id);
				model.put("itemDetail", i);
			}
		}
		else {
			model.put("isLoggedin", "true");
			if(null == idString)
			{
				model.put("postAction", "/item/new");
				model.put("itemDetail", new Item());
			}
			else
			{
				model.put("postAction", "/item/update");
				Long id = Long.parseLong(idString);
				Item i = itemRepo.getById(id);
				model.put("itemDetail", i);
			}

		}

		model.put("list", itemRepo.getAll());
		
		//das Template itemDetail verwenden und dann "anzeigen".
		return new ModelAndView(model, "productDetailTemplate");
	}
	
	
	
}


