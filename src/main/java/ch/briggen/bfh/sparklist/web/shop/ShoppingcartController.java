package ch.briggen.bfh.sparklist.web.shop;

import ch.briggen.bfh.sparklist.domain.shop.Item;
import ch.briggen.bfh.sparklist.domain.shop.ItemRepository;
import ch.briggen.bfh.sparklist.domain.shop.Shoppingcart;
import ch.briggen.bfh.sparklist.domain.shop.ShoppingcartRepository;
import ch.briggen.bfh.sparklist.domain.user.User;
import ch.briggen.bfh.sparklist.domain.user.UserRepo;
import ch.briggen.bfh.sparklist.web.register.RegisterController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.thymeleaf.templateparser.raw.IRawHandler;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

import javax.lang.model.element.Element;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.*;

import static ch.briggen.bfh.sparklist.domain.util.JdbcRepositoryHelper.getConnection;

public class ShoppingcartController implements TemplateViewRoute {



    ShoppingcartRepository repository = new ShoppingcartRepository();


    @Override
    public ModelAndView handle(Request request, Response response) throws Exception {
        if (request.cookie("UserID") != null) {
            long user_id = Long.parseLong(request.cookie("UserID"));
            int item_id = Integer.parseInt(request.queryParams("id"));
            if (item_id == 0) {
                response.redirect("/");
            }
            HashMap<String, Shoppingcart> model = new HashMap<String, Shoppingcart>();

            System.out.println(item_id);
            repository.createShoppingcart(item_id, user_id);
            Shoppingcart shoppingcart = repository.getShoppingcart(user_id);
            model.put("items", shoppingcart);
            return new ModelAndView(model, "shoppingListTemplate");
        }
        else{
            response.redirect("/");
            return null;
        }
    }
}



