package ch.briggen.bfh.sparklist.web.shop;

import java.util.Collection;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.shop.Item;
import ch.briggen.bfh.sparklist.domain.shop.ItemRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * WWW controller
 * Returns the whole list under "/".
 *
 */
public class ListManagementRootController implements TemplateViewRoute {

	ItemRepository repository = new ItemRepository();

	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {

		HashMap<String, Object> model = new HashMap<String, Object>();

		if (request.cookie("loggedin") == null) {
			model.put("list", repository.getAll());
			model.put("isLoggedin", "false");
			return new ModelAndView(model, "landingPageTemplate");
		}
		else {
			model.put("list", repository.getAll());
			model.put("isLoggedin", "true");
			return new ModelAndView(model, "landingPageTemplate");
		}
	}
}
