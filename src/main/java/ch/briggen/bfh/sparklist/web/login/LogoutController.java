package ch.briggen.bfh.sparklist.web.login;

import ch.briggen.bfh.sparklist.domain.user.UserRepo;
import ch.briggen.bfh.sparklist.domain.session.SessionRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

public class LogoutController implements TemplateViewRoute {

    private UserRepo userRepo = new UserRepo();
    private SessionRepository SessionRepo = new SessionRepository();

    @Override
    public ModelAndView handle(Request request, Response response) throws Exception {
        response.removeCookie("loggedin");
        response.removeCookie("UserID");
        response.redirect("/");
        return null;
    }
}
