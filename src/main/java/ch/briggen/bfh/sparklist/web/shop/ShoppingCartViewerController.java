package ch.briggen.bfh.sparklist.web.shop;

import ch.briggen.bfh.sparklist.domain.shop.Item;
import ch.briggen.bfh.sparklist.domain.shop.ItemRepository;
import ch.briggen.bfh.sparklist.domain.shop.Shoppingcart;
import ch.briggen.bfh.sparklist.domain.shop.ShoppingcartRepository;
import ch.briggen.bfh.sparklist.domain.user.User;
import ch.briggen.bfh.sparklist.domain.user.UserRepo;
import ch.briggen.bfh.sparklist.web.register.RegisterController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

public class ShoppingCartViewerController implements TemplateViewRoute {


    private final Logger log = LoggerFactory.getLogger(RegisterController.class);

    ShoppingcartRepository repository = new ShoppingcartRepository();

    @Override
    public ModelAndView handle(Request request, Response response) throws Exception {
        if (request.cookie("UserID") != null) {
        long user_id = Long.parseLong(request.cookie("UserID"));

        Shoppingcart shoppingcart= repository.getShoppingcart(user_id);


        HashMap<String, Shoppingcart> model = new HashMap<String,Shoppingcart>();

        if (shoppingcart == null){
            response.redirect("/");
            return null;
        }

        else {
            model.put("items", shoppingcart);


            return new ModelAndView(model, "shoppingListTemplate");
        }

    }
        else {
            response.redirect("/");
            return null;
        }
    }
}
