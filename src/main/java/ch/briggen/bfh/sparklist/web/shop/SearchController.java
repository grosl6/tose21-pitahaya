package ch.briggen.bfh.sparklist.web.shop;

import ch.briggen.bfh.sparklist.domain.shop.Item;
import ch.briggen.bfh.sparklist.domain.shop.ItemRepository;
import ch.briggen.bfh.sparklist.web.register.RegisterController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

import java.util.*;


public class SearchController implements TemplateViewRoute {

    private final Logger log = LoggerFactory.getLogger(RegisterController.class);

    ItemRepository repository = new ItemRepository();



    @Override
    public ModelAndView handle(Request request, Response response) throws Exception {
        String item = request.queryParams("searchitem");

        HashMap<String, Collection<Item>> model = new HashMap<String, Collection<Item>>();
        model.put("list", repository.searchItem(item));

        if (item.length() == 0){
            model.put("list", repository.getAll());
        }

        return new ModelAndView(model, "landingPageTemplate");
        }


    }

