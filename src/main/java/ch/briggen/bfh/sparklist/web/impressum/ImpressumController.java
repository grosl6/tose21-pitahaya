package ch.briggen.bfh.sparklist.web.impressum;

import ch.briggen.bfh.sparklist.domain.shop.Item;
import ch.briggen.bfh.sparklist.domain.shop.ItemRepository;
import ch.briggen.bfh.sparklist.web.register.RegisterController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

import java.util.Collection;
import java.util.HashMap;

public class ImpressumController implements TemplateViewRoute {
    private final Logger log = LoggerFactory.getLogger(RegisterController.class);

    ItemRepository repository = new ItemRepository();

    @Override
    public ModelAndView handle(Request request, Response response) throws Exception {

        HashMap<String, Object> model = new HashMap<String, Object>();

        if (request.cookie("loggedin") == null) {
            model.put("list", repository.getAll());
            model.put("isLoggedin", "false");
            return new ModelAndView(model, "aboutTemplate");
        }
        else {
            model.put("list", repository.getAll());
            model.put("isLoggedin", "true");
            return new ModelAndView(model, "aboutTemplate");
        }
    }

}
