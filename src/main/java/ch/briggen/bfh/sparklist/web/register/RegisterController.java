package ch.briggen.bfh.sparklist.web.register;
import ch.briggen.bfh.sparklist.domain.user.User;
import ch.briggen.bfh.sparklist.domain.user.UserRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

import java.util.HashMap;

public class RegisterController implements TemplateViewRoute {

    private final Logger log = LoggerFactory.getLogger(RegisterController.class);

    private UserRepo userRepo = new UserRepo();

    @Override
    public ModelAndView handle(Request request, Response response) throws Exception {
        String idString = request.queryParams("id");
        HashMap<String, Object> model = new HashMap<String, Object>();

        if(null == idString) {
            model.put("postAction", "/register/new");
            model.put("UserRegistration", new User());
        }

        else {
            model.put("postAction", "/login");
            Long id = Long.parseLong(idString);
            User u = userRepo.getUserById(id);
            model.put("UserRegistration", u);
        }

        return new ModelAndView(model, "registerTemplate");
    }

}
