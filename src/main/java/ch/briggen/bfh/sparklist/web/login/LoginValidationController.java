package ch.briggen.bfh.sparklist.web.login;

import ch.briggen.bfh.sparklist.domain.shop.ItemRepository;
import ch.briggen.bfh.sparklist.domain.user.User;
import ch.briggen.bfh.sparklist.domain.user.UserRepo;
import ch.briggen.bfh.sparklist.domain.session.SessionRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

import java.util.HashMap;

public class LoginValidationController implements TemplateViewRoute {

    private UserRepo userRepo = new UserRepo();
    private ItemRepository repository = new ItemRepository();
    private SessionRepository SessionRepo = new SessionRepository();

    @Override
    public ModelAndView handle(Request request, Response response) throws Exception {
        HashMap<String, Object> model = new HashMap<String, Object>();
        String email = request.queryParams("email");
        String password = request.queryParams("password");

        if (userRepo.validateUser(email, password)){
            model.put("isLoggedin", "true");
            model.put("list", repository.getAll());
            response.cookie("/", "loggedin", "true", 3600, true);
            response.cookie("/", "UserID", userRepo.getUserIDByEmail(email), 3600, true);
            SessionRepo.insert(userRepo.getUserIDByEmail(email));
            return new ModelAndView(model, "landingPageTemplate");
        }
        else {
            model.put("postAction", "/login/validation");
            model.put("UserValidation", new User());
            model.put("authentication", false);
            return new ModelAndView(model, "loginTemplate");
        }
        }
    }
