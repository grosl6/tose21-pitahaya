package ch.briggen.bfh.sparklist.web.login;

import ch.briggen.bfh.sparklist.domain.user.User;
import ch.briggen.bfh.sparklist.domain.user.UserRepo;
import spark.*;

import java.util.HashMap;

public class LoginRootController implements TemplateViewRoute {

    private UserRepo userRepo = new UserRepo();

    @Override
    public ModelAndView handle(Request request, Response response) throws Exception {
        String idString = request.queryParams("id");
        HashMap<String, Object> model = new HashMap<String, Object>();

        if(null == idString) {
            model.put("postAction", "/login/validation");
            model.put("UserValidation", new User());
        }

        return new ModelAndView(model, "loginTemplate");
    }
}
