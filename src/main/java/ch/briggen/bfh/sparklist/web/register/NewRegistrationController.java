package ch.briggen.bfh.sparklist.web.register;

import ch.briggen.bfh.sparklist.domain.user.User;
import ch.briggen.bfh.sparklist.domain.user.UserRepo;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

import java.util.HashMap;

public class NewRegistrationController implements TemplateViewRoute {

    private UserRepo userRepo = new UserRepo();

    @Override
    public ModelAndView handle(Request request, Response response) throws Exception {
        HashMap<String, Object> model = new HashMap<String, Object>();

        long id = Long.parseLong(request.queryParams("id"));
        String firstname = request.queryParams("firstname");
        String lastname = request.queryParams("lastname");
        String email = request.queryParams("email");
        String password = request.queryParams("password");

        User userDetail = new User(id, firstname, lastname, email, password);

        if (userRepo.checkIfEmailExists(email)){
            model.put("postAction", "/register/new");
            model.put("UserRegistration", new User());
            model.put("registrationFailed", true);
            return new ModelAndView(model, "registerTemplate");
        }
        id = userRepo.insertUser(userDetail);
        request.session().attribute("UserID", id);
        response.redirect("/");
        return null;
    }
}
