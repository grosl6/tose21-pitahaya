package ch.briggen.bfh.sparklist.web.shop;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.shop.Item;
import spark.Request;

class ItemWebHelper {
	@SuppressWarnings("unused")
	private final static Logger log = LoggerFactory.getLogger(ItemWebHelper.class);
	
	public static Item itemFromWeb(Request request)
	{
		return new Item(
				Long.parseLong(request.queryParams("itemDetail.id")),
				request.queryParams("itemDetail.name"),
				request.queryParams("itemDetail.imagepath"),
				Integer.parseInt(request.queryParams("itemDetail.quantity")),
				Double.parseDouble(request.queryParams("itemDetail.price")));
	}

}
